﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _63120237_Seminar2
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var websr = new ServiceReference1.HRMServiceClient();
                var poizvedba = (from x in websr.GetDepartmentList() select x.DepartmentGroupName).Distinct();
                DDL_DepartmentGroup.DataSource = poizvedba.ToList();
                DDL_DepartmentGroup.DataBind();

                string departmentGroupName = DDL_DepartmentGroup.SelectedItem.Value;

                var poizvedba2 = (from x in websr.GetDepartmentList()
                                  where x.DepartmentGroupName.Equals(departmentGroupName)
                                  select x.DepartmentName);

                DDL_Department.DataSource = poizvedba2.ToList();
                DDL_Department.DataBind();

                GV_Employee.AutoGenerateSelectButton = true;
                int depID = 0;

                for (int i = 0; i < websr.GetDepartmentList().Length; i++)
                {
                    if (websr.GetDepartmentList().ElementAt(i).DepartmentName.Equals(DDL_Department.SelectedItem.Value))
                    {
                        depID = websr.GetDepartmentList().ElementAt(i).DepartmentID;
                    }
                }

                var poizvedba3 = from x in websr.GetEmployeeList()
                                 where x.DepartmentID == depID
                                 select new { EmployeeID =  x.EmployeeID,
                                              name = x.FirstName + " " + x.LastName, 
                                              JobTitle = x.JobTitle, 
                                              HireDate = ((DateTime)x.HireDate).ToString("dd/MM/yyyy"), 
                                               
                                            };

                GV_Employee.DataSource = poizvedba3.ToList();
                GV_Employee.DataBind();

                if (DDL_Department.SelectedItem.Value.Equals("Sales") && GV_Employee.SelectedIndex != -1)
                {
                    B_Orders.Visible = true;
                }
                else
                {
                    B_Orders.Visible = false;
                }

                L_SaleStatistics1.Visible = false;
                L_SaleStatistics2.Visible = false;
                L_SaleStatistics3.Visible = false;
                GV_Orders.Visible = false;
            }
        }

        protected void DDL_DepartmentGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            var websr = new ServiceReference1.HRMServiceClient();
            string departmentGroupName = DDL_DepartmentGroup.SelectedItem.Value;

            var poizvedba2 = (from x in websr.GetDepartmentList()
                              where x.DepartmentGroupName.Equals(departmentGroupName)
                              select x.DepartmentName);

            DDL_Department.DataSource = poizvedba2.ToList();
            DDL_Department.DataBind();

            if (DDL_Department.SelectedItem.Value.Equals("Sales") && GV_Employee.SelectedIndex != -1)
            {
                B_Orders.Visible = true;
            }
            else
            {
                B_Orders.Visible = false;
            }
            L_SaleStatistics1.Visible = false;
            L_SaleStatistics2.Visible = false;
            L_SaleStatistics3.Visible = false;
            GV_Orders.Visible = false;
        }

        protected void DDL_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            DV_EmployeeDetails.Visible = false;
            var websr = new ServiceReference1.HRMServiceClient();
            int depID = 0;

            for (int i = 0; i < websr.GetDepartmentList().Length; i++)
            {
                if (websr.GetDepartmentList().ElementAt(i).DepartmentName.Equals(DDL_Department.SelectedItem.Value))
                {
                    depID = websr.GetDepartmentList().ElementAt(i).DepartmentID;
                }
            }

            var poizvedba3 = from x in websr.GetEmployeeList()
                             where x.DepartmentID == depID
                             select new
                             {   EmployeeID = x.EmployeeID,
                                 name = x.FirstName + " " + x.LastName,
                                 JobTitle = x.JobTitle,
                                 HireDate = ((DateTime)x.HireDate).ToString("dd/MM/yyyy"),
                                 
                             };
            GV_Employee.DataSource = poizvedba3.ToList();
            GV_Employee.DataBind();

            if (DDL_Department.SelectedItem.Value.Equals("Sales") && GV_Employee.SelectedIndex != -1)
            {
                B_Orders.Visible = true;
            }
            else
            {
                B_Orders.Visible = false;
            }
            L_SaleStatistics1.Visible = false;
            L_SaleStatistics2.Visible = false;
            L_SaleStatistics3.Visible = false;
            GV_Orders.Visible = false;
        }

        protected void GV_Employee_DataBound(object sender, EventArgs e)
        {
            GV_Employee.SelectedIndex = -1;
        }

        protected void DDL_Department_DataBound(object sender, EventArgs e)
        {
            GV_Employee.SelectedIndex = -1;

            DV_EmployeeDetails.Visible = false;

            var websr = new ServiceReference1.HRMServiceClient();
            int depID = 0;

            for (int i = 0; i < websr.GetDepartmentList().Length; i++)
            {
                if (websr.GetDepartmentList().ElementAt(i).DepartmentName.Equals(DDL_Department.SelectedItem.Value))
                {
                    depID = websr.GetDepartmentList().ElementAt(i).DepartmentID;
                }
            }

            var poizvedba3 = from x in websr.GetEmployeeList()
                             where x.DepartmentID == depID
                             select new
                             {   EmployeeID = x.EmployeeID,
                                 name = x.FirstName + " " + x.LastName,
                                 JobTitle = x.JobTitle,
                                 HireDate = ((DateTime)x.HireDate).ToString("dd/MM/yyyy"),
                                 
                             };
            GV_Employee.DataSource = poizvedba3.ToList();
            GV_Employee.DataBind();

            if (DDL_Department.SelectedItem.Value.Equals("Sales") && GV_Employee.SelectedIndex != -1)
            {
                B_Orders.Visible = true;
            }
            else
            {
                B_Orders.Visible = false;
            }
            L_SaleStatistics1.Visible = false;
            L_SaleStatistics2.Visible = false;
            L_SaleStatistics3.Visible = false;
            GV_Orders.Visible = false;
        }

        protected void GV_Employee_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.Pager) { e.Row.Cells[1].Visible = false; }
        }

        protected void GV_Employee_RowCreated(object sender, GridViewRowEventArgs e)
        {
        }

        protected void GV_Employee_SelectedIndexChanged(object sender, EventArgs e)
        {
            DV_EmployeeDetails.Visible = true;

            int empID = Convert.ToInt32(GV_Employee.SelectedRow.Cells[1].Text);

            var websr = new ServiceReference1.HRMServiceClient();
            
            var poizvedba = from x in websr.GetEmployeeList()
                            where x.EmployeeID == empID
                            select new { Name = x.FirstName + " " + x.LastName, 
                                         Gender = x.Gender, 
                                         Birthdate = ((DateTime)x.BirthDate).ToString("dd/MM/yyyy"), 
                                         Adress = x.Address+" "+x.PostalCode+" "+x.City+" "+x.State+" "+x.Country,
                                         Phone = x.PhoneNumber,
                                         Email = x.EmailAddress,
                                         HireDate = ((DateTime)x.HireDate).ToString("dd/MM/yyyy"),
                                         WorkTime = x.Shift,
                                         JobTitle = x.JobTitle,
                                         Shift = "Day ( "+x.ShiftStartTime+" - "+x.ShiftEndTime+" )",
                                         PayRate = x.PayRate+" €"
                            };
            DV_EmployeeDetails.DataSource = poizvedba.ToList();
            DV_EmployeeDetails.DataBind();

            if (DDL_Department.SelectedItem.Value.Equals("Sales") && GV_Employee.SelectedIndex != -1)
            {
                B_Orders.Visible = true;
            }
            else
            {
                B_Orders.Visible = false;
            }
            L_SaleStatistics1.Visible = false;
            L_SaleStatistics2.Visible = false;
            L_SaleStatistics3.Visible = false;
            GV_Orders.Visible = false;
        }

        protected void B_Orders_Click(object sender, EventArgs e)
        {
            L_SaleStatistics1.Visible = true;
            L_SaleStatistics2.Visible = true;
            L_SaleStatistics3.Visible = true;
            GV_Orders.Visible = true;

            int empID = Convert.ToInt32(GV_Employee.SelectedRow.Cells[1].Text);

            var websr = new ServiceReference1.HRMServiceClient();

            var poizvedva = (from x in websr.GetOrderList(empID)
                             select x.Customer).Distinct();

            L_SaleStatistics1.Text = poizvedva.ToList().Count().ToString();

            var poizvedba2 = (from x in websr.GetOrderList(empID)
                              select x.OrderNumber);

            L_SaleStatistics2.Text = poizvedba2.ToList().Count().ToString();

            var poizvedba3 = (from x in websr.GetOrderList(empID)
                              select x.SubTotal);

            int xx = 0;
            for (int i = 0; i < poizvedba3.ToList().Count(); i++)
            {
                xx += Convert.ToInt32(poizvedba3.ToList().ElementAt(i).ToString());
            }

            L_SaleStatistics3.Text = xx.ToString() + " €";

            var poizvedba4 = (from x in websr.GetOrderList(empID)
                              select new
                              {
                                  x.OrderNumber,
                                  OrderDate = ((DateTime)x.OrderDate).ToString("dd/MM/yyyy"),
                                  x.Customer,
                                  Amount = x.Amount,
                                  SubTotal = x.SubTotal+" €",
                                  Tax = x.TaxAmt+" €",
                                  Freight = x.Freight+" €",
                                  TotalDue = x.TotalDue+" €"
                              });

            GV_Orders.DataSource = poizvedba4.ToList();
            GV_Orders.DataBind();
        }
    }
}