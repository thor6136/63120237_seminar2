﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_63120237_Seminar2.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>HRM</h1>
        <hr>
        <br>
        Organizacijska enota:
        <asp:DropDownList ID="DDL_DepartmentGroup" runat="server" OnSelectedIndexChanged="DDL_DepartmentGroup_SelectedIndexChanged" AutoPostBack="True">
        </asp:DropDownList>
        <br />
        Oddelek:
        <asp:DropDownList ID="DDL_Department" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DDL_Department_SelectedIndexChanged" OnDataBound="DDL_Department_DataBound">
        </asp:DropDownList>
        <br />
        <hr>
        <br />
        <asp:GridView ID="GV_Employee" runat="server" OnDataBound="GV_Employee_DataBound" AllowPaging="true" OnRowCreated="GV_Employee_RowCreated" OnRowDataBound="GV_Employee_RowDataBound" OnSelectedIndexChanged="GV_Employee_SelectedIndexChanged" PageSize="5">
            <SelectedRowStyle BackColor="Green" />  
        </asp:GridView>
        <br />
        <asp:DetailsView ID="DV_EmployeeDetails" runat="server" Height="50px" Width="125px">
        </asp:DetailsView>
        <br />
        <asp:Button ID="B_Orders" runat="server" OnClick="B_Orders_Click" Text="Prikaži ustvarjen promet" />
        <br />
        <asp:Label ID="L_SaleStatistics1" runat="server" Text="Število strank:"></asp:Label>
        <br />
        <asp:Label ID="L_SaleStatistics2" runat="server" Text="Label"></asp:Label>
        <br />
        <asp:Label ID="L_SaleStatistics3" runat="server" Text="Label"></asp:Label>
        <br />
        <br />
        <asp:GridView ID="GV_Orders" runat="server" PageSize="20" AllowPaging="true">
        </asp:GridView>
    </div>
    </form>
</body>
</html>
